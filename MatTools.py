# Author: Jorge Orozco
#         Universidad de Concepción, Facultad de Ingeniería
#         E-mail: joorozco@udec.cl

"""

*** MatTools.py ***

Contains:
Functions for mathematical operations.

External dependencies:
numpy       -Numpy Python extension. http://numpy.org/
             Version: Numpy 1.22.3

Internal dependencies:
cmath       -cMath Python extension. https://docs.python.org/3/library/cmath.html

Changelog:
Date          Name              Change
__ _          __ _              ____ _
02/06/2022    Jorge Orozco      Initial release

References:
Short     Author,Year     Title
___ _     _________ _     ___ _

"""
# Imports
import numpy as np
from cmath import pi

# Auxiliary funtions
deg2rad=pi/180
rad2deg=180/pi

# Converts hexadecimal to decimal degrees
def hexa2dec(coord):
    'coord=[hour, minute, second] to coord[deg]'
    
    dec=coord[0]+((coord[1])/60)+((coord[2])/3600)
    return dec

# Converts a 3 elements array into a 3x3 skew matrix
def skew3(vector):   
    'vector is a 3 elements array or list'

    skew_matrix=np.array([[0,-vector[2], vector[1]],
                          [vector[2],0,-vector[0]],
                          [-vector[1],vector[0],0]])
    return skew_matrix

# Converts a 3 elements vector into a 4x4 skew matrix
def skew4(vector):
    'vector is a 3 elements array or list'

    skew_matrix=np.array([[0,vector[2],-vector[1],vector[0]],
                          [-vector[2],0, vector[0],vector[1]],
                          [vector[1],-vector[0],0, vector[2]],
                          [-vector[0],-vector[1],-vector[2],0]])
    return skew_matrix

# Converts a 3x3 Director Cosine Matrix into a quaternion
def mat2quat(dcm):  
    'dcm= 3X3 Director Cosine Matrix... to its respective quaternion [q1,q2,q3,q4]'

    q11=dcm[0][0]; q12=dcm[0][1]; q13=dcm[0][2]
    q21=dcm[1][0]; q22=dcm[1][1]; q23=dcm[1][2]
    q31=dcm[2][0]; q32=dcm[2][1]; q33=dcm[2][2]

    q4=0.5*((1+q11+q22+q33)**0.5)
    q1=(q23-q32)/(4*q4)
    q2=(q31-q13)/(4*q4)
    q3=(q12-q21)/(4*q4)

    quaternion=np.array([q1,q2,q3,q4])
    return quaternion

# Converts a quaternion into a 3x3 Director Cosine Matrix
def quat2mat(quat):
    'quat= quaternion [q1,q2,q3,q4]... into a 3x3 Director Cosine Matrix' 

    q1=quat[0] ; q2=quat[1] ; q3=quat[2] ; q4=quat[3]
    dcm11=(q1**2) - (q2**2 )- (q3**2) + (q4**2) 
    dcm22=-(q1**2) + (q2**2 )- (q3**2) + (q4**2) 
    dcm33=-(q1**2) - (q2**2 )+ (q3**2) + (q4**2)
    dcm12=2*((q2*q1)+(q3*q4)) ; dcm21=2*((q2*q1)-(q3*q4))
    dcm13=2*((q3*q1)-(q2*q4)) ;dcm31=2*((q3*q1)+(q2*q4))
    dcm23=2*((q3*q2)+(q1*q4)) ; dcm32=2*((q3*q2)-(q1*q4))
    dcm=np.array([[dcm11,dcm12,dcm13],[dcm21,dcm22,dcm23],[dcm31,dcm32,dcm33]])
    return dcm

# Converts all elements less than 1e-8 into 0
def check(vector):
    'Receives any length vector'
    for i in range(len(vector)):
        if abs(vector[i])<=10**-8:
            vector[i]=0
        else:
            vector[i]=vector[i]
    return vector

# Converts a vector into its normalised vector
def normalise(vector):
    'Normalises a vector'

    norm=np.linalg.norm(vector)
    unit=(1/norm)*vector
    return unit

# Converts a 3 elements vector into a diagonal matrix
def vec2mat(vector):
    'Converts a 3 elements vector into a 3x3 diagonal matrix'

    v1=vector[0]; v2=vector[1]; v3=vector[2]
    mat=np.array([[v1,0,0],[0,v2,0],[0,0,v3]])
    return mat

# Conjugates a quaternion by multiplying its vector elements by -1
def q_conjugate(q):
    'Conjugates a [q1,q2,q3,q4] quaternion'

    q1=-q[0]
    q2=-q[1]
    q3=-q[2]
    q4=q[3]
    conj=np.array([q1,q2,q3,q4])
    return conj

# Uses Hamilton's multiplication for quaternions
def hamilton(q1,q2):
    'Multiplies two quaternion by Hamiltons method'

    a1=q1[3]; b1=q1[0]; c1=q1[1]; d1=q1[2]
    a2=q2[3]; b2=q2[0]; c2=q2[1]; d2=q2[2]
    q_4=a1*a2 - b1*b2 - c1*c2 - d1*d2
    q_1=a1*b2 + b1*a2 + c1*d2 - d1*c2
    q_2=a1*c2 - b1*d2 + c1*a2 + d1*b2
    q_3=a1*d2 + b1*c2 - c1*b2 + d1*a2
    return np.array([q_1,q_2,q_3,q_4])

# Rotation of a 3 elements vector by a given quaternion to both directions
def q_rot(v,q,invs):
    """
    Rotates a vector by given quaternion [q1,q2,q3,q4]
    If invs=1 rotates in opposite direction
    """
    
    if invs==0:
       q_conj=q_conjugate(q)
       q_v=np.array([v[0],v[1],v[2],0])
       first=hamilton(q_conj,q_v)
       new=hamilton(first,q)
    elif invs==1:
       q_conj=q_conjugate(q)
       q_v=np.array([v[0],v[1],v[2],0])
       first=hamilton(q,q_v)
       new=hamilton(first,q_conj)
    return np.array([new[0],new[1],new[2]])

def yawpitchroll(rot_vector):
    'gets dcm from angles of rotation in 312 sequence or yaw pitch roll. Receives angles in deg'
    phi=rot_vector[0]*deg2rad ; sin_phi=np.sin(phi) ; cos_phi=np.cos(phi)
    theta=rot_vector[1]*deg2rad ; sin_theta=np.sin(theta) ; cos_theta=np.cos(theta)
    psy=rot_vector[2]*deg2rad ; sin_psy=np.sin(psy) ; cos_psy=np.cos(psy)

    matrix_phi=np.array([[cos_phi,sin_phi,0],[-sin_phi,cos_phi,0],[0,0,1]])  #yaw
    matrix_theta=np.array([[cos_theta,0,sin_theta],[0,1,0],[-sin_theta,0,cos_theta]])  #pitch
    matrix_psy=np.array([[1,0,0],[0,cos_psy,-sin_psy],[0,sin_psy,cos_psy]])  #roll

    mult1=np.dot(matrix_theta,matrix_phi)
    mult2=np.dot(matrix_psy,mult1)
    
    return mult2

def q2ypr(q):
    matrix=quat2mat(q)

    yaw=np.arctan2((matrix[0][1]),(matrix[0][0]))
    #print(yaw*rad2deg)

    pitch=np.arctan2(matrix[0][2],np.sqrt(((matrix[1][2])**2)+((matrix[2][2])**2)))
#print(pitch*rad2deg)

    roll=np.arctan2(matrix[1][2],matrix[2][2])
#print((roll*rad2deg))
    orientation=np.array([yaw*rad2deg,pitch*rad2deg,roll*rad2deg])

    return orientation

def angle_vector(v1,v2):
    v1_norm=np.linalg.norm(v1)
    v2_norm=np.linalg.norm(v2)
    v_dot=np.dot(v1,v2)
    cos=v_dot/(v1_norm*v2_norm)
    angle=np.arccos(cos)

    return angle*rad2deg