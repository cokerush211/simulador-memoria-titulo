
import matplotlib.pyplot as plt
import numpy as np
from Clock import Clock
from Planet import Planet
from Rocket import Rocket
import GeoTools as Geo
import MatTools as Mat
from Atmosphere import Atmosphere
from cmath import pi

# Auxiliary funtions
deg2rad=pi/180
rad2deg=180/pi


# ------ Configuration of time simulation related data------#
Start=0; End=60; dt=0.2; Steps_num=int(End/dt)                 
Steps=np.linspace(Start,End,Steps_num)

#___________________Initial data to be input________________#  # All of this to be obtained from external file
# Setting of date and time of launch      
Year=2022  
Month= 5
Day= 16
Hour= 0
Minute=0
Second=0   

date=[Year,Month,Day,Hour,Minute,Second]; j_date=Clock().julian_day(date)
gmst_0=Clock().gmst(j_date,1)

Platform_coord=np.array([-36,-70,19])
Platform_vel_enu=np.array([0,0,0])
Orientation=np.array([135,45,0])
Angular_rotation_rocket=np.array([0,0,0])

Temperature=25

r_ecef_0=Geo.geo2ecef(Platform_coord)
v_ecef_0=Geo.enu2ecef(Platform_coord,Platform_vel_enu)
q_b2ecef_0=Mat.normalise(Geo.q_body_ecef(Orientation,Platform_coord))
w_ecef_0=Mat.q_rot(Angular_rotation_rocket,q_b2ecef_0,0)

sin_lat=np.sin(Platform_coord[0]*deg2rad); cos_lat=np.cos(Platform_coord[0]*deg2rad)
sin_long=np.sin(Platform_coord[1]*deg2rad); cos_long=np.cos(Platform_coord[1]*deg2rad)
dcm_ecef2enu=np.array([[-sin_long,cos_long,0],[-cos_long*sin_lat,-sin_long*sin_lat,cos_lat],[cos_long*cos_lat,sin_long*cos_lat,sin_lat]])
q_ecef2enu=Mat.mat2quat(dcm_ecef2enu)

Earth=Planet(gmst_0)
Environment=Atmosphere(Temperature)
Sistema=Rocket(r_ecef_0,v_ecef_0,q_b2ecef_0,w_ecef_0)

# ------ Auxiliary timer (not an object) --------#
t=Start
Time=[]

for i in range(len(Steps)):     

    Earth.update(dt)

    Sistema.update_gmst(Earth.gmst)
    Sistema.update_mass_related()
    Sistema.update_pos_vel(q_ecef2enu,Platform_coord)
    Sistema.get_atmosphere(Environment.give_dens(Sistema.coord[2]),Environment.give_press(Sistema.coord[2]),Environment.give_v_sonic(Sistema.coord[2]))
    Sistema.get_components()
    Sistema.update_forces_torques()
    Sistema.save_data()  

   # Counter adds time of simulation
    Time.append(t)

    Sistema.RK4(dt)
    #Sistema.attackangle()
    # Conditional to stop process amid simulation
    if Sistema.coord[2]<=0:
        break
    
    #Sistema.attackangle()
    Sistema.update_time(dt)

    # Counter updates time of simulation to stop simulation
    t+=dt

print(Sistema.hist_alt)
plt.plot(Sistema.hist_range,Sistema.hist_alt)

plt.figure()
plt.plot(Time,Sistema.hist_orient)
plt.show()
 
    
