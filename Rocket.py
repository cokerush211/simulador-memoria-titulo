import numpy as np
from cmath import pi
import GeoTools as Geo
import MatTools as Mat
from Aerodynamics import Aerodynamics
from Engine import Engine
from Gravitational import gravity

class Rocket(object):
    def __init__(self,r_ecef_0,v_ecef_0,q_b2ecef_0,w_ecef_0):

    ###-----Initial-----###
        self.r_ecef=r_ecef_0
        self.v_ecef=v_ecef_0
        self.q_b2ecef=q_b2ecef_0
        self.w_ecef=w_ecef_0

    ###-----Initial time related data----###
 
        self.sim_time=0
        self.burnt_time=3.15
    # --------- Initial mass related data --------------#
        mass_0=9.48                             # [kg]    #obtained from external
        inertia_0=np.array([0.009584,0.666,0.666]) # [kg m2] #obtained from external
        xcm_0=0.535                             # [m]     #obtained from external
        
        self.mass=mass_0                        # Rocket initial mass    [kg]
        self.inertia=inertia_0                  # Rocket initial inertia [kg m2]
        self.xcm=xcm_0                          # Centre of pressure     [m]

    ###-----Set at 0 for calculation---##
        
        self.gmst=0

        self.coord=np.zeros(3)
        self.r_eci=np.zeros(3)
        self.v_eci=np.zeros(3)

        self.density=0
        self.press_amb=0
        self.v_sonic=0
    
        self.v=0          # Velocity magnitude
        self.mach=0
        self.cd=0
        self.cl=0
        self.xcp=0
        self.drag=0
        self.lift=0
        self.thrust=0
        self.mass_flux=0
        self.cm2cp=np.zeros(3)

        self.forces_b=np.zeros(3)
        self.forces_ecef=np.zeros(3)
        self.forces_eci=np.zeros(3)
        self.torques_b=np.zeros(3)
        self.torques_ecef=np.zeros(3)
        self.torques_eci=np.zeros(3)

        self.torque_gg_b=np.zeros(3)

        self.w_b=np.zeros(3)
        self.q_plat2b=np.zeros(4) 
        self.orientation=np.zeros(3)

        self.angle=0
       
        self.range=0

    ###--- Historics ---###
        
        self.hist_r_ecef=[]
        self.hist_v_ecef=[]
        self.hist_q_b2ecef=[]
        self.hist_w_ecef=[]

        self.hist_sim_time=[]
        self.hist_mass=[]
        self.hist_inertia=[]
        self.hist_xcm=[]
        self.hist_gmst=[]

        self.hist_coord=[]
        self.hist_alt=[]
        self.hist_lat=[]
        self.hist_long=[]
        self.hist_r_eci=[]
        self.hist_v_eci=[]

        self.hist_density=[]
        self.hist_press_amb=[]
        self.hist_v_sonic=[]

        self.hist_v=[]
        self.hist_mach=[]
        self.hist_cd=[]
        self.hist_cl=[]
        self.hist_xcp=[]
        self.hist_drag=[]
        self.hist_lift=[]
        self.hist_thrust=[]
        self.hist_mass_flux=[]
        self.hist_cm2cp=[]

        self.hist_forces_b=[]
        self.hist_forces_ecef=[]
        self.hist_forces_eci=[]
        self.hist_torques_b=[]
        self.hist_torques_ecef=[]
        self.hist_torques_eci=[]
 
        self.hist_torque_gg_b=[]

        self.hist_w_b=[]

        self.hist_q_plat2b=[]

        self.hist_orient=[]
  
        self.hist_angle=[]

        self.hist_range=[]
    
    def update_gmst(self,gmst):
        self.gmst=gmst
    
    def update_mass_related(self):
        'Arranges inertia and centre of mass depending on simulation time'
       
        if self.sim_time<=self.burnt_time:
            self.inertia=np.array([0.009584,0.666,0.666]) # [kg m2] #obtained from external
            self.xcm=534/1000                          # [m]     #obtained from external
        else:
            self.inertia=np.array([0.00654,0.402,0.402]) # [kg m2] #obtained from external
            self.xcm=424/1000                          # [m]     #obtained from external

    def update_pos_vel(self,q_ecef2enu,platform_coord):
        self.coord=Geo.ecef2geo(self.r_ecef)               # LLA coordinates
        self.r_eci=Geo.ecef2eci(self.gmst,self.r_ecef)     # ECI coordinates
        self.v_eci=Geo.ecef2eci_v(self.gmst,self.r_ecef,self.v_ecef)  # ECI velocity
  
        self.w_b=Mat.q_rot(self.w_ecef,self.q_b2ecef,1)    # angular velocity in bodyframe
   
        self.q_b2enu=Mat.hamilton(self.q_b2ecef,q_ecef2enu) # Quaternion from bodyframe to platform ENU coordinates
        self.q_plat2b=Mat.q_conjugate(self.q_b2enu)         # Quaternion from platform ENU coordinates to bodyframe

        self.orientation=Mat.q2ypr(self.q_plat2b)           # Yaw pitch roll from platform

        platform_ecef=Geo.geo2ecef(platform_coord)          
        platform_current=self.r_ecef-platform_ecef
        range=Geo.ecef2enu(platform_coord,platform_current)
        self.range=np.linalg.norm(range)                    # Magnitude Range from platform ENU coordinates

    def get_atmosphere(self,density,press_amb,v_sonic):
        self.density=density
        self.press_amb=press_amb
        self.v_sonic=v_sonic

    def get_components(self):     
        self.v=np.linalg.norm(self.v_ecef)
        if self.v==0:
            self.mach=0
        else:
            self.mach=self.v/self.v_sonic

        Aero=Aerodynamics(self.mach,self.sim_time)
        self.cd=Aero.cd
        self.cl=Aero.cl
        self.xcp=Aero.xcp

        area=pi*0.25*(0.0889**2)      # [m2]
       
        # Conditionals to avoid RUNTIME WARNING 
        ## angle of attack so little that we asume normal=lift and axial=drag
        if self.cd==0:                          
            self.drag=0                                # [N]
        else: 
            self.drag=0.5*self.density*self.cd*area*(self.v**2)  # [N]

        if self.cl==0:
            self.lift=0                                # [N]
        else:
            self.lift=0.5*self.density*self.cl*area*(self.v**2)  # [N]

        Motor=Engine(self.sim_time,self.press_amb)
        self.thrust=Motor.thrust
        self.mass_flux=Motor.mass_flux
        
        cm2cp=self.xcm-self.xcp                      # [m] 
        self.cm2cp=np.array([cm2cp,0,0])

    def update_forces_torques(self):
        forces_x=self.thrust-self.drag
        forces_y=0
        forces_z=self.lift

        self.forces_b=np.array([forces_x,forces_y,forces_z])
        self.forces_ecef=Mat.q_rot(self.forces_b,self.q_b2ecef,0)
        self.forces_eci=Geo.ecef2eci(self.gmst,self.forces_ecef)

        self.torques_b=np.cross(self.cm2cp,self.forces_b)
        self.torques_ecef=Mat.q_rot(self.torques_b,self.q_b2ecef,0)
        self.torques_eci=Geo.ecef2eci(self.gmst,self.torques_ecef)
 
    # def attackangle(self):
    #     vector_body_body=np.array([1,1,0])
    #     vector_body_ecef=Mat.q_rot(vector_body_body,self.q_b2ecef,0)
    #     vector_body_eci=Geo.ecef2eci(self.gmst,vector_body_ecef)

    #     self.angle=Mat.angle_vector(self.v_eci,vector_body_eci)

  
    # def attackangle(self):
    #     body_body=np.array([1,0,0])
    #     body_plat=Mat.q_rot(body_body,self.q_plat2b,1)
    #     v_body=Mat.q_rot(self.v_ecef,self.q_b2ecef,1)
    #     v_plat=Mat.q_rot(v_body,self.q_b2ecef,1)

    #     angle=Mat.angle_vector(body_plat,v_plat)
    
    def dynamics(self,x):

        r_eci=x[0:3]
        v_eci=x[3:6]
        q_body_ecef=x[6:10]
        w_ecef=x[10:13]
        mass=x[13]

        g=gravity(r_eci)
        alt_aux=self.coord[2]

        if alt_aux<=0:
            # Position time derivative as vehicle laying on the ground
            r_dot_eci=(Geo.ecef2eci_v(self.gmst,r_ecef=Geo.eci2ecef(self.gmst,r_eci),
                                     v_ecef=np.zeros(3)))
            # Velocity time derivative as vehicile laying on the ground
            v_dot_eci= g
                    
        else:
            # Position time derivative as current velocity
            r_dot_eci=v_eci
            # Velocity time derivative from sum of forces in inertial system
            v_dot_eci=(self.forces_eci*(1/mass)) + g
        
        j=Mat.vec2mat(self.inertia); j_inv=np.linalg.inv(j)
        # r_ecef=Geo.eci2ecef(self.gmst,r_eci)
        r_ecef_b=Mat.q_rot(self.r_ecef,q_body_ecef,1)
        mass_earth=5.972*(10**24)
        univers_constant=6.67384*(10**(-11))
        r=np.linalg.norm(self.r_ecef)
        self.torque_gg_b=(3*mass_earth*univers_constant/(r**5))*np.cross(r_ecef_b,(np.dot(j,r_ecef_b)))
        
        w_b=Mat.q_rot(w_ecef,q_body_ecef,1)
        w_dot_b=np.dot(j_inv,(np.cross(-w_b,(np.dot(j,w_b))) + (self.torques_b+self.torque_gg_b)))

        skew_w_ecef=Mat.skew4(w_ecef)

        w_dot_ecef=Mat.q_rot(w_dot_b,q_body_ecef,0)
        q_dot_body_ecef=0.5*skew_w_ecef.dot(q_body_ecef)
        
        mass_dot=-self.mass_flux

        # fx analog to first x
        f_traj=np.concatenate((r_dot_eci,v_dot_eci))
        f_att=np.concatenate((q_dot_body_ecef,w_dot_ecef))
        f_mov=np.concatenate((f_traj,f_att))
        m_dot_aux=np.array([mass_dot])                 # Auxiliary mass_dot to avoid float error in Numpy
        fx=np.concatenate((f_mov,m_dot_aux))
        return fx

    def RK4(self,dt):
        'Integrates fx from dynamics to propagate'
        
        # Prepares concatenation of current variables
        traj=np.concatenate((self.r_eci,self.v_eci))
        att=np.concatenate((self.q_b2ecef,self.w_ecef))
        mov=np.concatenate((traj,att))
        mass=np.array([self.mass])                  # Auxiliary mass_dot to avoid float error in Numpy
        x=np.concatenate((mov,mass))

        #------------Runge-Kutta 4 sequence----------#
        k1=self.dynamics(x)
        xk2=x + 0.5*dt*k1

        k2=self.dynamics(xk2)
        xk3=x+ 0.5*dt*k2

        k3=self.dynamics(xk3)
        xk4=x+ dt*k3

        k4=self.dynamics(xk4)

        new_x= x + (dt/6)*(k1 + 2*k2 + 2*k3 + k4)


        r_eci=new_x[0:3]; self.r_ecef=Geo.eci2ecef(self.gmst,r_eci)
        v_eci=new_x[3:6]; self.v_ecef=Geo.eci2ecef_v(self.gmst,r_eci,v_eci)
        q_b2ecef=new_x[6:10]; self.q_b2ecef=Mat.normalise(q_b2ecef)
        w_ecef=new_x[10:13]; self.w_ecef=w_ecef
        self.mass=new_x[13]

    def save_data(self):
        self.hist_r_ecef.append(self.r_ecef)
        self.hist_v_ecef.append(self.v_ecef)
        self.hist_q_b2ecef.append(self.q_b2ecef)
        self.hist_w_ecef.append(self.w_ecef)

        self.hist_sim_time.append(self.sim_time)
        self.hist_mass.append(self.mass)
        self.hist_inertia.append(self.inertia)
        self.hist_xcm.append(self.xcm) 
        self.hist_gmst.append(self.gmst)

        self.hist_coord.append(self.coord)
        self.hist_alt.append(self.coord[2])
        self.hist_lat.append(self.coord[0])
        self.hist_long.append(self.coord[1])
        self.hist_r_eci.append(self.r_eci)
        self.hist_v_eci.append(self.v_eci)
 
        self.hist_density.append(self.density)
        self.hist_press_amb.append(self.press_amb)
        self.hist_v_sonic.append(self.v_sonic)
      
        self.hist_v.append(self.v)
        self.hist_mach.append(self.mach)
        self.hist_cd.append(self.cd)
        self.hist_cl.append(self.cl)
        self.hist_xcp.append(self.xcp)

        self.hist_drag.append(self.drag)
        self.hist_lift.append(self.lift)
        self.hist_thrust.append(self.thrust)
        self.hist_mass_flux.append(self.mass_flux)
        self.hist_cm2cp.append(self.cm2cp)
  
        self.hist_forces_b.append(self.forces_b)
        self.hist_forces_ecef.append(self.forces_ecef)
        self.hist_forces_eci.append(self.forces_eci)
        self.hist_torques_b.append(self.torques_b)
        self.hist_torques_ecef.append(self.torques_ecef)
        self.hist_torques_eci.append(self.torques_eci)
    
        self.hist_torque_gg_b.append(self.torque_gg_b)

        self.hist_w_b.append(self.w_b)

        self.hist_q_plat2b.append(self.q_plat2b)

        self.hist_orient.append(self.orientation[1])

        self.hist_angle.append(self.angle)

        self.hist_range.append(self.range)

    def update_time(self,dt):
        'Updates simulation time from simulation time step'

        self.sim_time+=dt