# Author: Jorge Orozco
#         Universidad de Concepción, Facultad de Ingeniería
#         E-mail: joorozco@udec.cl

"""

*** Aerodynamics.py ***

Contains:
Aerodynamics module containing aerodynamic coefficients.

External dependencies:
numpy       -Numpy Python extension. http://numpy.org/
             Version: Numpy 1.22.3
scipy       -Scipy Python extension. https://www.scipy.org
             Version: Scipy 1.6.2

Internal dependencies:
cmath       -cMath Python extension. https://docs.python.org/3/library/cmath.html

Changelog:
Date          Name              Change
__ _          __ _              ____ _
02/06/2022    Jorge Orozco      Initial release

References:
Short       Author,Year           Title
___ _       _________ _           ___ _
[Valle22]   Vallejos,2022         Mejora del alcance de un cohete chaff
[Barro67]   Barrowman,1967        The Practical Calculation of the Aerodynamic Characteristic of Slender Finned Vehicles
[Box09]     Box/Bishop/Hunt,2009  Estimating the dynamic and aerodynamic parameters of 
                                  passively controlled high power rockets for flight simulation
[Ketch92]   Ketchledge,1992       Active Guidance and Dynamic Flight Mechanics for Model Rockets
[Fleem01]   Fleeman,2001          Tactical Missile Design

"""
# Imports
import numpy as np
from scipy import interpolate
from cmath import pi

# Auxiliary funtions
deg2rad=pi/180
rad2deg=180/pi

class Aerodynamics(object):
    def __init__(self,mach,time):                          
        self.time=time


        #______________________Drag Coefficient____________________________#
        
        # m= Mach number [-], [Valle22]          # To be obtained from external file
        m = np.array([0.1,0.4,0.7,0.8,0.9,1,1.1,1.15,1.2,1.25,1.3,1.4,1.6,1.9,2.2,2.5,3])  
       
        # cd= Drag coefficient [-], [Valle22]    # To be obtained from external file
        cd = np.array([0.32498,0.32302,0.32639,0.32957,0.34603,
                      0.45078,0.50512,0.525,0.55998,0.53465,0.51829,
                      0.50535,0.47411,0.42844,0.38992,0.3556,0.30924,])
  
        # Interpolation from data to create a curve function that describes cd behaviour
        spline = interpolate.splrep(m, cd)           

        self.mach=mach     # [-]
        
        # Obtains cd from interpolation and given Mach number
        self.cd=interpolate.splev(self.mach, spline)  
       
        #_____________Lift Coefficient and Centre of Pressure_____________#
        
        # Define an angle of attack for simulation between 0° to 10° [Fleem01] (Fleeman suggests 10° instead of 15°)
       
        ### This one is a try for cyclic angle of attack according to Khalil and his team
        if self.time<=3.15:
            n=self.time/0.2
            angle=0.001*(-1**n)
        else:
            angle=0        
                   # [°]
        
        
        #angle=0   ###this one is the one I want to asume considering ideal stability
        
        
        alpha=angle*deg2rad   # [rad]
        
        # Coefficient of normal force aproximation for subsonic regim, [Box09] and [Barro67]
        # Values will be given in external file with example draw   
        X_b=205.35  # [mm]
        X_f=856     # [mm]
        X_c=936     # [mm]
        
        l_n=205.35  # [mm]
        l_b=730     # [mm]
        l_r=76.32   # [mm]
        l_t=33.6722 # [mm]
        l_m=46.6440 # [mm]
        l_c=62      # [mm]
        l_s=41.75   # [mm]

        d_n=88.9    # [mm]
        d_b=88.9    # [mm]
        d_f=88.9    # [mm]
        d_u=88.9    # [mm]
        d_d=93      # [mm]
        
        fins=4      # Number of fins

        # Cone [Box09]:
        Cn_alpha_cone=2      # [-]
        Xcp_cone=0.466*l_n   # [mm]

        # Body, [Box09]:
        Cn_alpha_body=(l_b/(pi*0.25*d_b))*alpha   # [-]
        Xcp_body=X_b + 0.5*l_b                    # [mm]
        
        # Tail, [Box09]:
        Cn_alpha_tail=2*(((d_d/d_n)**2) - ((d_u/d_n)**2))  # [-]
        Xcp_tail= X_c + (l_c/3)* (1 + 1/(1+(d_u/d_n)))     # [mm]

        # Fins [Box09]:
        Kfb=1 + ((0.5*d_f)/(l_s + 0.5*d_f))                                                           # [-]
        Cn_alpha_fins=(Kfb*4*fins)*(((l_s/d_n)**2)/(1 + (np.sqrt(1+(2*l_m/(l_r+l_t))**2))))           # [-]
        Xcp_fins= X_f+((l_m/3)*((l_r + 2*l_t)/(l_r+l_t)))+((1/6)*(l_r + l_t-((l_r*l_t)/(l_r+l_t))))   # [mm]
         
        # Results, [Box09]:
        Cn_sum=Cn_alpha_cone + Cn_alpha_body + Cn_alpha_tail + Cn_alpha_fins # [-] 
        cn=Cn_sum*alpha                                                      # [-]

        # Centre of pressure location from nose tip using [Box09]:
        self.xcp=((Cn_alpha_cone*Xcp_cone + Cn_alpha_body*Xcp_body + Cn_alpha_tail*Xcp_tail + Cn_alpha_fins*Xcp_fins)/Cn_sum)/1000  # [m]

        # Ketchledge correction, [Ketch92] and [Box09]
        if self.mach<0.8:
            # Subsonic case
            self.cl=cn/(np.sqrt(1-(self.mach**2)))   # [-]

        elif self.mach>=0.8 and self.mach<=1.2:
            # Transonic case
            self.cl=cn/(np.sqrt(1-(0.8**2)))         # [-]

        else:
            # Supersonic case
            self.cl=cn/(np.sqrt(-1+(self.mach**2)))  # [-]




#[Gray64]    Gray,1964             SUMMARY REPORT O.N AERODYNAMIC CHARACTERISTICS OF STANDARD  MODELS HB-1 AND HB-2---> Cn_sum=0.5*alpha
#[Fleem01]   Fleeman,2001          Tactical Missile Design  ---> Cn_body=2*alpha                                 


# test=Aerodynamics(2)

# print(test.cl)