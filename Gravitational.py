# Author: Jorge Orozco
#         Universidad de Concepción, Facultad de Ingeniería
#         E-mail: joorozco@udec.cl

"""

*** Gravitational.py ***

Contains:
Functions to obtain gravitational acceleration

External dependencies:
MatTools    -Self-written python module containing mathematical functions

Internal dependencies:

Changelog:
Date          Name              Change
__ _          __ _              ____ _
02/06/2022    Jorge Orozco      Initial release

References:
Short       Author,Year    Title
___ _       _________ _    ___ _
[Curt20]    Curtis,2020    Orbital Mechanics for Engineering Students

"""
# Imports
import MatTools as Mat
import numpy as np

# Function for gravitational acceleration with constant magnitude, [Curt20]
def gravity(r_eci):
    
    # Unit vector for current position in Earth Centered Inertial
    norm=Mat.normalise(r_eci) # [-]
    
    # Gravitational acceleration with constant magnitude
    g=-9.81*norm              # [m/s2]
    
    return g

